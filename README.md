# Docker and Kubernetes

### Day1 
#### Introduction to Containerization
 - Containerization
-  History of Containers
-  Namespaces and Cgroups
-  Containers vs Virtual Machines
-  Types of Containers
-  Introduction to Docker
-  Docker Architecture
-  Container Lifecycle
-  Docker CE vs Docker EE

#### The Docker Engine
 - Topics:
-  Docker Engine
-  Configuring Logging Drivers
-  Docker Terminology
-  Port Binding
-  Detached vs Foreground Mode
-  Docker CLI
-  Docker Exec
-  Restart Policy


#### Image Management and Registry
 - Dockerfile
-  Dockerfile Instructions
-  Build Context
-  Docker Image
-  Docker Registry
-  Pushing image to private container registry

#### Storage in Docker
 - Docker Storage
-  Types of Persistent Storage
-  Volumes
-  Bind Mounts
-  Storage Drivers
-  Device Mapper
-  Docker Clean Up




## Day 2
### Kubernetes Architecture

 - Introduction to Kubernetes Master
-  kube-apiserver
-  etcd key-value store
-  kube-scheduler
-  kube-controller-manager
-  Introduction to Node Components of Kubernetes
-  kubelet
-  kube-proxy
-  kubectl


### Kubernetes Cluster

- Introduction to Pods
-  Why do we need a Pod?
-  Pod Lifecycle
-  Working with Pods to manage multiple containers
-  What is a Node?
-  Kubectl basic commands

### Expose App, Scale App and Update App in Kubernetes

 - What is a Service?
-  Labels and Selectors
-  Deployment Controller
-  Replica Set
-  Replication Controller
-  Scaling out a deployment using replicas
-  Horizontal pod autoscaler
-  Load balancing
-  Rolling Update
-  Ingress and its types


### Managing State with Deployments
 - Pod management policies: Ordered Ready, Parallel
-  Update strategies: On Delete, Rolling Update
-  Cluster DNS
-  Headless services
-  Persistent Volumes









